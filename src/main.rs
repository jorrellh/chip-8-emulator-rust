use rand::Rng;

fn main() {
  let x = 0xFFFF;
  let V = [0xFFFF, 0xFEFE, 0xBEEF];
  println!("{}",x);
  println!("{}",V[0x0]);

  // Initialize the CPU and memory.
  let mut cpu = CPU {index: 0x0000, pc: 0x0000, sp: 0x0000, stack: [0x0000; 16], v: [0xA4; 16]};
  let mut mem = Mem { data: [0x52A1; 4096] };

  // Pass in memory and the cycles to do.
  cpu.execute(3, &mem);


}

//struct Display {}


struct CPU {
	index: u16,
	pc: u16,
	sp: u8,
	stack:[u16; 16],
	v:[u8; 16],

}

impl CPU {

	fn execute(&mut self, mut cycles: u16, mem: &Mem) {
		println!("executed!");
		while cycles > 0 {
			// define bytes and separate by 4 bits.
			let bytes: u16 = self.fetch(cycles, mem);
			let first_byte: u16 = bytes >> 8;
			let byte1: u8 = CPU::u16_to_u8(first_byte);

			let second_byte: u16 = bytes & 255;
			let byte2: u8 = CPU::u16_to_u8(second_byte);

			let nibble1: u8 = byte1 >> 4;
      let nibble2: u8 = byte1 & 0b00001111;
			let nibble3: u8 = byte2 >> 4;
			let nibble4: u8 = byte2 & 0b00001111;

			println!("First nibble: {}", nibble1);
			println!("Second nibble: {}", nibble2);
			println!("Third nibble: {}", nibble3);
			println!("Fourth nibble: {}", nibble4);
			println!("First byte: {}", byte1);
			println!("Second byte: {}", byte2);
      println!("Stack: {:?}", self.stack);

			println!("VX value: {}", self.v[usize::from(nibble2)]);
			println!("VY value: {}", self.v[usize::from(nibble3)]);

			match nibble1 {
				0x0 => {
          if byte2 == 0xEE {
            // Last item of the stack or last inserted item?
            self.pc = self.stack[self.stack.len() - 1];
            if self.sp != 0 {
              self.sp = self.sp - 1;
            }

            println!("Test 0xEE")
          } else {
            println!("Not implemented 0x0")
          }
        },

				0x1 => self.pc = CPU::get_addr(nibble2, nibble3, nibble4),
				0x2 => println!("Not implemented 0x2"),
				0x3 => {
          if self.v[usize::from(nibble2)] == byte2 {
            self.pc = self.pc + 2;
          }
        },
				0x4 => {
          if self.v[usize::from(nibble2)] != byte2 {
            self.pc = self.pc + 2;
          }
        },
				0x5 => {
          if self.v[usize::from(nibble2)] == self.v[usize::from(nibble3)] {
            self.pc = self.pc + 2;
          }
        },
				0x6 => self.v[usize::from(nibble2)] = byte2,
				0x7 => self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] + byte2,
				0x8 => {

					if nibble4 == 0 {
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)];
					} else if nibble4 == 1 {
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] | self.v[usize::from(nibble3)];
					} else if nibble4 == 2 {
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] & self.v[usize::from(nibble3)];
					} else if nibble4 == 3 {
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] ^ self.v[usize::from(nibble3)];
					} else if nibble4 == 4 {

						let sum : u16 = u16::from(self.v[usize::from(nibble2)]) + u16::from(self.v[usize::from(nibble3)]);

						if sum > 255 {
							self.v[0xF] = 1;

						} else {
							self.v[0xF] = 0;
						}

						// Not sure if meant to be outside here..
						self.v[usize::from(nibble2)] = CPU::u16_to_u8(sum & 0x00FF);
					} else if nibble4 == 5 {

						if self.v[usize::from(nibble2)] > self.v[usize::from(nibble3)] {
							self.v[0xF] = 1;

							// If this is meant to overflow, then move outside the if/else statement.
							self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] - self.v[usize::from(nibble3)];

						} else {
							self.v[0xF] = 0;
						}
					} else if nibble4 == 6 {
						if self.v[usize::from(nibble2)] & 0b00000001 == 1 {
							self.v[0xF] = 1;
						} else {
							self.v[0xF] = 0;
						}
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] / 2;
					} else if nibble4 == 7 {
						if self.v[usize::from(nibble3)] > self.v[usize::from(nibble2)]{

							self.v[0xF] = 1;

							// If this is meant to overflow, then move outside the if/else statement.
							self.v[usize::from(nibble2)] = self.v[usize::from(nibble3)] - self.v[usize::from(nibble2)];

						} else {

							self.v[0xF] = 0;

						}
					} else if nibble4 == 0xE {

						if self.v[usize::from(nibble2)] & 0b10000000 == 1 {

							self.v[0xF] = 1;

						} else {

							self.v[0xF] = 0;

						}

						// Not sure if meant to be outside here.. It may overflow.
						self.v[usize::from(nibble2)] = self.v[usize::from(nibble2)] * 2;

					} else {
						println!("Undocumented behaviour");
					}

				},
				0x9 => {
					if self.v[usize::from(nibble2)] != self.v[usize::from(nibble3)] {
						self.pc = self.pc + 2;
					}
				},
				0xA => self.index = CPU::get_addr(nibble2, nibble3, nibble4),
				0xB => {
					self.pc = CPU::get_addr(nibble2, nibble3, nibble4);
					self.v[0x0] = 0x0A;
					self.pc = self.pc + u16::from(self.v[0x0]);

				},
				0xC => {
					let mut rng = rand::thread_rng();
					let r: u8 = rng.gen();
					self.v[usize::from(nibble2)] = r & byte2;

				},
				0xD => println!("Not implemented 0xD"), // Display specific instructions
				0xE => println!("Not implemented 0xE"),
				0xF => println!("Not implemented 0xF"),
				_ => println!("Ain't special")


			}
			for x in 0..16 {
				println!("V{} values: {}", x, self.v[x]);
			}

			println!("Program Counter: {}", self.pc);

			println!("I: {}", self.index);

			println!("Data: {}", bytes);
			cycles = cycles - 1;
		}
	}

	fn fetch(&mut self, cycles: u16, mem: &Mem) -> u16 {
		let data: u16 = mem.data[usize::from(self.pc)];
		self.pc += 1;
		return data;
	}

	fn u16_to_u8(v: u16) -> u8 {
		let mut byte: u8 = 0;
		for _x in 0..v {
			byte = byte + 1;

		}
    	return byte;
	}

	fn get_addr(n1: u8, n2: u8, n3: u8) -> u16 {

		let e1 : u16 = u16::from(n1) << 8;
		let e2 : u16 = u16::from(n2) << 4;
		let e3 : u16 = u16::from(n3);

		let addr : u16 = e1 + e2 + e3;

		return addr;

	}

}

struct Mem {
	data:[u16; 4096]

}

impl Mem {
	// read two bytes
	fn operator(&self, address: u16) -> u16 {
		return self.data[usize::from(address)];
	}
}
